
#make sure you have  Node 6.9.0 or higher, together with NPM 3 or higher.
Node -v
NPM -v

#Run the command:
sudo command
npm install -g @angular/cli

#If you have the permissing error, visit:
https://docs.npmjs.com/getting-started/fixing-npm-permissions

#Install
npm install

#Run the server
ng serve  or  ng serve --host 0.0.0.0 --port 4201
