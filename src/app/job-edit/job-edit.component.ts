import {
  Component,
  OnInit,

  Input,
  Inject
} from '@angular/core';


import {
  Job,
} from '../models';

import {IOService} from '../io.service';


import {
  MAT_DIALOG_DATA
} from '@angular/material';
import {
  NgxCarousel
} from 'ngx-carousel';
import {
  DomSanitizer,
  SafeUrl
} from '@angular/platform-browser';

@Component({
  selector: 'app-job-edit',
  templateUrl: './job-edit.component.html',
  styleUrls: ['./job-edit.component.css']
})
export class JobEditComponent implements OnInit {
  public carouselOne: NgxCarousel;
  public loading = false;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any, private io: IOService, private _sanitizationService: DomSanitizer) {
    this.url = this._sanitizationService.bypassSecurityTrustResourceUrl('');
  }

  @Input() job: Job;
  public url;
  public iframeHeight = 'no-height';
  public opacity = '';
  public pageGrey = '';
  public message = '';
  public tmpTag = '';

  ngOnInit() {
    console.log(this.data);
    this.io.get(Job, this.io.APIURL + '/jobs/' + this.data.job).subscribe(job => {
      console.log(job);
      this.job = job;
    });

  }

  videoUrl(videoUrl): void {
    this.iframeHeight = 'auto-height';
    this.pageGrey = 'overlay-grey';
    this.opacity = 'half-opacity';
    this.url = this._sanitizationService.bypassSecurityTrustResourceUrl(
      'https://player.vimeo.com/video/' + videoUrl + '?autoplay=1color=3f3f3f&byline=0&portrait=0');

  }

  closeVideo(): void {
    this.iframeHeight = 'no-height';
    this.opacity = '';
    this.pageGrey = '';
    this.url = this._sanitizationService.bypassSecurityTrustResourceUrl('');
  }

  getJob(id): void {


  }

  getPage(id): void {


  }

  saveJob() {
    console.log(this.job);
    this.loading = true;
    this.io.update(this.job, this.io.APIURL + '/jobs/' + this.job._id).subscribe(updatedJob => {
      console.log(updatedJob);
      this.loading = false;
      this.message = 'saved!';
    });
  }

  addTag() {
    if (!this.job.tags) {
      this.job.tags = [];
    }
    if (this.tmpTag !== '' && !this.job.tags.includes(this.tmpTag)) {
      this.job.tags.push(this.tmpTag);
    }
    this.tmpTag = '';
    console.log(this.job.tags);
  }

  deleteTag(tag) {
    if (tag && this.job.tags) {
      const position = this.job.tags.indexOf(tag);
      if (position > -1) {
        this.job.tags.splice(position, 1);
      }
    }

  }


}
