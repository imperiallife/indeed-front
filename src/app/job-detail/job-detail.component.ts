import {
    Component,
    OnInit,

    Input,
    Inject
} from '@angular/core';


import {
    Job,
} from '../models';

import {IOService} from '../io.service';


import {
    MAT_DIALOG_DATA
} from '@angular/material';
import {
    NgxCarousel
} from 'ngx-carousel';
import {
    DomSanitizer,
    SafeUrl
} from '@angular/platform-browser';
@Component({
    selector: 'app-job-detail',
    templateUrl: './job-detail.component.html',
    styleUrls: ['./job-detail.component.css']
})
export class JobDetailComponent implements OnInit {
    public carouselOne: NgxCarousel;

    constructor(@Inject(MAT_DIALOG_DATA) public data: any, private io: IOService, private _sanitizationService: DomSanitizer) {
        this.url = this._sanitizationService.bypassSecurityTrustResourceUrl('');
    }

    @Input() job: Job;
    public url;
    public iframeHeight = 'no-height';
    public opacity = '';
    public pageGrey = '';
    ngOnInit() {
      console.log(this.data);
      this.io.get(Job, this.io.APIURL + '/jobs/' + this.data.job).subscribe(job => {
        console.log(job);
        this.job = job;
      });

    }
    videoUrl(videoUrl): void {
        this.iframeHeight = 'auto-height';
        this.pageGrey = 'overlay-grey';
        this.opacity = 'half-opacity';
        this.url = this._sanitizationService.bypassSecurityTrustResourceUrl(
          'https://player.vimeo.com/video/' + videoUrl + '?autoplay=1color=3f3f3f&byline=0&portrait=0');

    }
    closeVideo(): void {
        this.iframeHeight = 'no-height';
        this.opacity = '';
        this.pageGrey = '';
        this.url = this._sanitizationService.bypassSecurityTrustResourceUrl('');
    }
    getJob(id): void {



    }

    getPage(id): void {


    }


}
