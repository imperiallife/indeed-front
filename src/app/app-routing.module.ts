import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {RouterModule, Routes} from '@angular/router';
import {JobListComponent} from './job-list/job-list.component';
import {JobEditComponent} from './job-edit/job-edit.component';

const routes: Routes = [

  {
    path: 'job/:job', component: JobListComponent
  },
  {
    path: 'job-edit/:job', component: JobEditComponent
  },
   {
    path: 'page/:page', component: JobListComponent
  },
  {path: '', component: JobListComponent},

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
    CommonModule
  ],
  exports: [RouterModule]

})
export class AppRoutingModule {
}
