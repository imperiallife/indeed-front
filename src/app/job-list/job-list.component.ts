import {
  Component,
  OnInit,
  ViewChild,

  ElementRef,
  ChangeDetectorRef,

} from '@angular/core';


import {
  Job,
} from '../models';

import {IOService} from '../io.service';

import {
  JobDetailComponent
} from '../job-detail/job-detail.component';
import {
  JobEditComponent
} from '../job-edit/job-edit.component';
import {
  MatDialog
} from '@angular/material';

import {
  NgxCarousel,
  NgxCarouselStore
} from 'ngx-carousel';
import {

  ActivatedRoute,

} from '@angular/router';
import {
  Location,
  LocationStrategy,
  PathLocationStrategy
} from '@angular/common';


declare const ga: Function;
declare var jQuery: any;

@Component({
  selector: 'app-job-list',
  templateUrl: './job-list.component.html',
  styleUrls: ['./job-list.component.css'],
  providers: [Location, {
    provide: LocationStrategy,
    useClass: PathLocationStrategy
  }],


})


export class JobListComponent implements OnInit {

  public jobs: Job[] = [];
  public search: String = '';
  public loading: Boolean = false;
  public searched: Boolean = false;


  public carouselOne: NgxCarousel;
  public carouselDetail: NgxCarouselStore[] = [];

  public selectedIndex = 0;


  public detailId: number;
  public pageId: number;


  selectedIndexChange(data) {
    jQuery('.button-collapse').sideNav('hide');
    this.selectedIndex = data;

  }

  onmoveFn(data: NgxCarouselStore, index) {

    this.carouselDetail[index] = data;
  }

  /* This will be triggered after carousel viewed */
  afterCarouselViewedFn(data, index) {

    this.carouselDetail[index] = data;

  }


  showConditonDetail(id) {
    console.log(id);
    const dialogRef = this.dialog.open(JobDetailComponent, {
      data: {
        job: id
      },
      height: '90vh',
      width: '100%'
    });
  }
  editConditonDetail(id) {
    console.log(id);
    const dialogRef = this.dialog.open(JobEditComponent, {
      data: {
        job: id
      },
      height: '90vh',
      width: '100%'
    });
    dialogRef.afterClosed().subscribe(() => {
      this.io.list(Job, this.io.APIURL + '/jobs_search?search=' + this.search).subscribe(jobs => {
        this.processJobs(jobs);
        this.loading = false;
      });
    });
  }


  location: Location;

  constructor(private io: IOService,
              private cdr: ChangeDetectorRef,
              private element: ElementRef,
              public dialog: MatDialog,
              private route: ActivatedRoute,
              location: Location) {

    this.location = location;

    this.route.params.subscribe(res => this.detailId = res.job);

  }


  ngOnInit() {


    // User screen size
    const screenHeight = window.screen.height;
    const screenWidth = window.screen.width;

    // Actual space available in navigator
    const actualHeight = window.innerHeight;
    const actualWidth = window.innerWidth;

    this.carouselOne = {
      grid: {
        xs: 2,
        sm: 2,
        md: 4,
        lg: 4,
        all: 0
      },
      slide: 2,
      speed: 400,
      interval: 100000,
      point: {
        visible: false
      },
      load: 100,
      touch: false,
      loop: false,
      custom: 'banner'
    };


  }

  totalCounts(data) {
    let total = 0;

    data.forEach((d) => {
      total++;
    });

    return total;
  }

  filterJobs(search) {
    this.loading = true;
    console.log('search: ' + search);
    this.io.list(Job, this.io.APIURL + '/jobs_search?search=' + search).subscribe(jobs => {
      this.processJobs(jobs);
      this.loading = false;
      this.searched = true;
    });
  }

  deleteJob(job) {
    console.log(job);
    this.loading = true;
    job.isArchived = true;
    this.io.update(job, this.io.APIURL + '/jobs/' + job._id).subscribe(updatedJob => {
      console.log(updatedJob);
      this.io.list(Job, this.io.APIURL + '/jobs_search?search=' + this.search).subscribe(jobs => {
        this.processJobs(jobs);
        this.loading = false;
      });
    });
  }

  processJobs(jobs) {
    this.jobs = [];
    for (const singleJob of jobs) {

      if (singleJob.isArchived !== true) {
        console.log(singleJob.isArchived);
        this.jobs.push(singleJob);
      }
    }
  }


}
