import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {HttpClientModule} from '@angular/common/http';
import { HttpModule } from '@angular/http';
import {AppComponent} from './app.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatButtonModule, MatCheckboxModule} from '@angular/material';
import {MatTabsModule} from '@angular/material/tabs';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatDialogModule} from '@angular/material';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatIconModule} from '@angular/material/icon';


import {NgxCarouselModule} from 'ngx-carousel';

import {JobListComponent} from './job-list/job-list.component';
import {IOService} from './io.service';
import {AppRoutingModule} from './app-routing.module';
import {JobDetailComponent} from './job-detail/job-detail.component';
import {JobEditComponent} from './job-edit/job-edit.component';
import {FilterPipe, SortByPipe, HighlightPipe} from './pipes';
@NgModule({
  declarations: [
    AppComponent,
    JobListComponent,
    JobDetailComponent,
    JobEditComponent,
    FilterPipe,
    SortByPipe,
    HighlightPipe

  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatCheckboxModule,
    MatTabsModule,
    AppRoutingModule,
    HttpClientModule,
    HttpModule,
    MatDialogModule,
    MatProgressSpinnerModule,
    MatFormFieldModule,
    MatInputModule,
    MatAutocompleteModule,
    MatIconModule,
    NgxCarouselModule,
    MatSidenavModule,
    FormsModule,
    ReactiveFormsModule

  ],
  entryComponents: [
    JobDetailComponent,
    JobEditComponent
  ],
  providers: [IOService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
