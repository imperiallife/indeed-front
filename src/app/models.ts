export class Job {
    _id: string;
    url: string;
    slug: string;
    title: string;
    company: string;
    location: string;
    text: string;
    html: string;
    createTime: string;
    tags: string[];
    isArchived: boolean;
}
