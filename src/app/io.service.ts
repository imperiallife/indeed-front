import {Injectable} from '@angular/core';
import { Headers, Http, Response } from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/Rx';

@Injectable()
export class IOService {
  private headers = new Headers({'Content-Type': 'application/json'});
    public APIURL = 'http://35.182.197.74:3000/api';
    constructor(
      private http: Http,
    ) {}

  /* Generic CRUD Services */
  private handleError (error: Response | any) {
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }

  public get(model: any, route: string): Observable<any> {
    // You'll need to add the APIURL manually unless you
    // base your request on something like the practice.url
    const object = new model();
    return this.http.get(route, {headers: this.headers})
      .map(response => {
        const json = response.json();
        Object.keys(json).forEach(function (key) {
          object[key] = json[key];
        });
        return object;
      }).catch(this.handleError);
  }

  public list(model: any, route: string, ): Observable<any> {
    // You'll need to add the APIURL manually unless you
    // base your request on something like the practice.url
    const object = new model();
    return this.http.get(route, {headers: this.headers})
      .map(response => {console.log(response);
        const json = response.json(),
          output = [];
        for (const item of json) {
          const obj = new model();
          Object.keys(item).forEach(key => {
            obj[key] = item[key];
          });
          output.push(obj);
        }
        return output;
      }).catch(this.handleError);
  }

  public create(object: any, route: string) {
    const url = route;
    if ('url' in object && object['url'] !== typeof 'undefined') {
      throw new Error('Object already created. Try update instead.');
    }
    return this.http.post(url, JSON.stringify(object), {headers: this.headers})
      .map(response => {
        const json = response.json();
        Object.keys(json).forEach(function (key) {
          object[key] = json[key];
        });
        return object;
      }).catch(this.handleError);
  }

  public update(object: any, route: string): Observable<any> {
    return this.http.patch(route, JSON.stringify(object), {headers: this.headers})
      .map(response => {
        const json = response.json();
        Object.keys(json).forEach(function (key) {
          object[key] = json[key];
        });
        return object;
      })
      .catch(this.handleError);
  }

  public remove(object: any): Observable<any> {
    if (!('url' in object) || object['url'] === typeof 'undefined') {
      throw new Error('Object is not yet created. Nothing to delete.');
    }
    return this.http.delete(object['url'], {headers: this.headers})
      .map(response => {
        return object;
      })
      .catch(this.handleError);
  }


}
